-------------------------------------------------------------------
-- Game includes
-------------------------------------------------------------------

local functions = require("openfun-core.functions")
local Player = require("player")
local Sector = require("sector")
local text = require("text")
-------------------------------------------------------------------
-- Variables
-------------------------------------------------------------------

local field_with = 18
local field_height = 16
local tx = 0
local ty = 0
local seed = 10
local width = 0
local height = 0
local network_timer = 0
local network_timer_base = 1/15

local time = 0

tile = {
  width = 32,
  height = 32,
}

image = {
  black = functions.generateBox(tile.width,tile.height,{0,0,0},25),
  blue = functions.generateBox(tile.width,tile.height,{0,0,255}),
  green = functions.generateBox(tile.width,tile.height,{0,255,0}),
  white = functions.generateBox(tile.width,tile.height,{255,255,255}),
  red = functions.generateBox(tile.width,tile.height,{255,0,0}),
  yellow = functions.generateBox(tile.width,tile.height,{255,255,0}),
  azure = functions.generateBox(tile.width,tile.height,{0,255,255})
}

local bg = functions.generateBox(128,32,{50,50,50},25)
local bg2 = functions.generateBox(128,64,{30,30,30},25)

tile[0] =  image.black
tile[1] =  image.green

local players = {}
local shoots = {}
local map = {}

-------------------------------------------------------------------
-- Game itself
-------------------------------------------------------------------

function love.load()
  players = {}
  map = Sector:init(field_with,field_height,seed)
    players[1] = Player:init()
end

function love.update(dt)
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  time = time + dt

  for i,v in ipairs(players) do
    if i == 1 then
      v:update(dt)
       v:check(dt,map.map,players)
      if v.health > 0 then
        v:control(dt,map.map)
      end
    else
      v:update(dt)
      v:check(dt,map.map,players)
    end
  end
end

function love.keypressed(key, scancode)
  players[1]:keypressed(key,scancode)
  if scancode == "r" and 1 > players[1].health then
    local x = math.random(map.width-2)+1
    local y = math.random(map.height-2)+1
    players[1]:reset(x,y,5)
  end
end

function love.draw()
  map:draw()

  y = 168
  x = width - 138
  love.graphics.draw(bg,x,10)
  love.graphics.print(text.game_info,x+10,20)
  love.graphics.draw(bg2,x,52)
  love.graphics.print(text.time..functions.round(time)..text.seconds,x+10,62)
  love.graphics.print(text.map_seed..seed,x+10,80)
  love.graphics.print(text.fps..tostring(love.timer.getFPS( )),x+10,98)
  love.graphics.draw(bg,x,y-42)
  love.graphics.print(text.players,x+10,y-32)
  for i,v in ipairs(players) do
    v:draw()
    if v.health > 0 then
      v:gui(x,y)
      y = y + 74
    end
  end
end
