return {
  lives = "Lives:",
  shoots = "Shoots:",
  kills = "Kills:",
  deaths = "Deaths:",
  players = "Players",
  game_info = "Game info",
  seconds = "s",
  time = "Time:",
  map_seed = "Map seed:",
  fps = "FPS:",
  ping = "Ping:"
}
