local functions = require("openfun-core/functions")
require("class")

local sector = class()

function sector:init(w,h,seed)
  local w = w or 16
  local h = h or 16
  local seed = seed or os.time()
  local tmp = {}
  setmetatable(tmp,sector)
  math.randomseed(seed)
  local obstacles = math.random(functions.round((w*h*4)/(w+h)))

  tmp.width = w
  tmp.height = h
  tmp.map = {}
  for x=1,w do
    tmp.map[x] = {}
    for y=1,h do
      tmp.map[x][y] = {}
      tmp.map[x][y]["color"] = "black"
      if y == 1 or x == 1 or x == w or y == h then
        tmp.map[x][y]["tile"] = 1
      else
        tmp.map[x][y]["tile"] = 0
      end
    end
  end

  for i=1,obstacles do
    local tmp_x = math.random(w)
    local tmp_y = math.random(h)
    tmp.map[tmp_x][tmp_y]["tile"] = 1
  end

  return tmp
end

function sector:draw(tx,ty,tile_width,tile_height,scale,rotation)
  local tx = tx or 0
  local ty = ty or 0
  local tile_width = tile_width or 32
  local tile_height = tile_height or 32
  local scale = scale or 1
  local rotation = rotation or 0

  for x=1,self.width do
    for y=1,self.height do
      if self.map[x][y]["tile"] == 1 then
        love.graphics.draw(image["white"],x*tile_width,y*tile_height)
      end
    end
  end

end

return sector
