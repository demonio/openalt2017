# -*- coding: utf-8 -*-
#!/usr/bin/python

# -----------------------------------------------------------------
# imports
# -----------------------------------------------------------------

import os
import SocketServer
import sys
import argparse
from time import time, sleep
import datetime
import random
from Queue import *
import threading
from functions import *

# -----------------------------------------------------------------
# Definitions
# -----------------------------------------------------------------

commands = {
    "players" : {
        "desc" : "Display all players",
        "command" : list_players,
        "params" : "None",
    },
    "register" : {
        "desc" : "Register player to server",
        "command" : register,
        "params" : "Name x y",
    },
    "move" : {
        "desc" : "Get players with their color and position",
        "command" : move,
        "params" : "id x y",
    },
    "shoot" : {
        "desc" : "Set your position",
        "command" : do_shoot,
        "params" : "id x y direction",
    },
    "dmg": {
        "desc" : "Decrease health by given number",
        "command" : dmg,
        "params" : "id amount",
    },
    "reset": {
        "desc" : "Reset player position and health",
        "command" : reset,
        "params" : "id x, y, health",
    }
}

arguments = {
    "address" : {
        "short" : "a",
        "desc" : "Server address",
        "required" : False,
        "default" : "localhost"
    },
    "udp" : {
        "short" : "u",
        "desc" : "UDP port",
        "required" : False,
        "default" : 25001
    },
    "tcp" : {
        "short" : "t",
        "desc" : "TCP port",
        "required" : False,
        "default" : 25000
    },
}

# -----------------------------------------------------------------
# Main
# -----------------------------------------------------------------

if __name__ == '__main__':
    ## Set char set ##
    reload(sys)
    sys.setdefaultencoding('utf-8')

    parser = argparse.ArgumentParser(
        description='Openalt game server')
    for k,v in arguments.iteritems():
        parser.add_argument("-" + v["short"], "--" + k,
            help=v["desc"], required=v["required"], default = v["default"])
    args = parser.parse_args()

    # TODO
    address = (args.address, args.tcp)
    address2 = (args.address, args.udp)
    server = SocketServer.TCPServer(address, TCPHandler)
    server2 = SocketServer.UDPServer(address2, UDPHandler)

    #TCP thread
    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True)  # don't hang on exit
    t.start()

    #UDP thread
    t2 = threading.Thread(target=server2.serve_forever)
    t2.setDaemon(True)
    t2.start()

    #Updater thread
    t3 = threading.Thread(target=updater)
    t3.setDaemon(True)
    t3.start()

    set_commands(commands)

    while 1:
        try:
            # DO THINGS
            command = raw_input(">")
            #TODO move to functions
            if command.lower() == "exit":
                print("Exiting")
                server.socket.close()
                server2.socket.close()
                sys.exit()
            else:
                print(use(command))
        except KeyboardInterrupt:
            # quit
            server.socket.close()
            server2.socket.close()
            sys.exit()
