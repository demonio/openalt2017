import os
import SocketServer
import sys
import argparse
from time import time, sleep
import datetime
import random
from Queue import *
import threading
import time

commands = {}
clients = []
shoots = []
shoots_total = 0

def python_to_lua(tmp):
    #value = str(tmp)
    #value = value.replace(":", "=")
    typed_list = {}
    for key, value in tmp.items():
        if type(value) == str:
            typed_list[key] = '"'+value+'"'
        elif type(value) == bool:
            typed_list[key] = str(value).lower()
        elif type(value) == list:
            typed_list[key] = str(value).replace("[", "{").replace("]", "}")
        else:
            typed_list[key] = str(value)
    return "{{{}}}".format(", ".join("{} = {}".format(key, typed_list[key]) for key in typed_list))


class TCPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request.recv(1024).strip()
        #print(data)
        if data.find("Connection test") > -1:
            self.request.send(data.upper())
        else:
            self.request.send(use(data))
        return


class UDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        #print(data)
        socket = self.request[1]
        value = use(data)
        socket.sendto(value, self.client_address)

def updater():
    global shoots, clients
    while True:
        startTime = time.time()

        for i in shoots:
            if i.timer > 0:
                i.timer -= 0.1
            else:
                shoots.remove(i)
        endTime = time.time() - startTime
        sleep(0.1 - endTime)

# -----------------------------------------------------------------
#  Classes
# -----------------------------------------------------------------

class client():

    def __init__(self, values):
        #name x y
        self.name = values[0]
        self.id = 0
        self.shoots = {}
        self.x = int(values[1])
        self.y = int(values[2])
        self.health = 5
        self.game = 0
        self.wins = 0
        self.games = 0
        self.kills = 0
        self.deaths = 0
        self.timer = 5
        self.color = ""
        print("Added new player " + self.name)

    def set_position(self,x,y):
        self.x = x
        self.y = y
        self.timer = 5

    def registered():
        return

    def get_info(self):
        tmp = {}

        tmp["name"] = self.name
        tmp["health"] = self.health
        tmp["id"] = self.id
        tmp["x"] = self.x
        tmp["y"] = self.y
        tmp["kills"] = self.kills
        tmp["deaths"] = self.deaths
        tmp["color"] = self.color

        #print(tmp)
        return python_to_lua(tmp)

class shoot():

    def __init__(self,values):
        #id x y direction
        self.player = int(values[0])
        self.id = 0
        self.x = int(values[1])
        self.y = int(values[2])
        self.dir = values[3]
        self.timer = 0.2

    def get_shoot(self):
        tmp = {}

        tmp["player"] = self.player
        tmp["id"] = self.id
        tmp["x"] = self.x
        tmp["y"] = self.y
        tmp["dir"] = self.dir

        #print(tmp)
        return python_to_lua(tmp)


# -----------------------------------------------------------------
# Game server commands
# -----------------------------------------------------------------

def list_players(values):
    tmp = ""
    for i in clients:
        tmp += i.name+", "
    return tmp[:-2]

def register(values):
    #Name x y
    clients.append(client(values))
    index = len(clients)-1
    clients[index].id = index
    tmp_color = random.sample(xrange(20,225), 3)
    clients[index].color = tmp_color
    return clients[index].get_info()+"--"

def move(values):
    #id x y
    clients[int(values[0])].set_position(int(values[1]), int(values[2]))
    tmp_string = "{ player = { "
    for i in clients:
        if i.health > -1:
            tmp_string += i.get_info()+","
    tmp_string += "}, shoots = {"
    for i in shoots:
            tmp_string += i.get_shoot()+","
    return tmp_string + "} }--"

def do_shoot(values):
    #id x y direction
    global shoots_total
    shoots.append(shoot(values))
    index = len(shoots)-1
    shoots_total += 1
    shoots[index].id = shoots_total
    return "DONE"

def dmg(values):
    #shooter target amount
    if clients[int(values[1])].health > 0:
        clients[int(values[1])].health -= int(values[2])
        if clients[int(values[1])].health < 1:
            clients[int(values[0])].kills += 1
            clients[int(values[1])].deaths += 1
    return "DONE"

def reset(values):
    #id x y health
    clients[int(values[0])].x = int(values[1])
    clients[int(values[0])].y = int(values[2])
    clients[int(values[0])].health = int(values[3])
    return "DONE"


# -----------------------------------------------------------------
# General functions
# -----------------------------------------------------------------

def usage():
    tmp = ""
    for i,v in commands.iteritems():
        tmp += "Command: " + i.ljust(20) + "\t Params: " + v["params"].ljust(20) + "\t Description: " +  v["desc"] + "\n"
    return tmp

def use(data):
    if not data:
        return ""
    tmp = data.split()
    command = tmp[0]
    del tmp[0]
    #print(command, tmp)
    if command.lower() == "help":
        return usage()
    try:
        return commands[command.lower()]["command"](tmp)
    except:
        return "Command not found"

def set_commands(data):
    global commands
    #print(data)
    commands = data
