local path = (...):match("(.-)[^%.]+$")
local functions = require(path.."functions")
local network = {}
local server = {}
local socket = require "socket"
local udp = socket.udp()
local tcp = socket.tcp()

-- Globals to locals
local require = require
local pairs = pairs
local love = love
local ipairs = ipairs
local print = print
local loadstring = loadstring
local tonumber = tonumber
local type = type

function network.load ()
    if love.filesystem.isFile("server.address") then
        server.address = love.filesystem.read("server.address")
    else
        server.address = "localhost"
        love.filesystem.newFile("server.address")
        love.filesystem.write("server.address", server.address)
    end
    server.address = "100.65.65.229"
    if love.filesystem.isFile("server.tcp") then
        server.tcp = love.filesystem.read("server.tcp")
        server.udp = love.filesystem.read("server.udp")
    else
        server.tcp = 25000
        server.udp = 25001
        love.filesystem.newFile("server.tcp")
        love.filesystem.write("server.tcp", server.tcp)
        love.filesystem.newFile("server.udp")
        love.filesystem.write("server.udp", server.tcp)
    end
    --DEBUG
    --server.address = "localhost"
    udp = socket.udp()
    udp:settimeout(0)
    udp:setpeername(server.address,25001)
end

function network.connect(port)
    tcp = socket.tcp()
    --print(server.address,port)
    tcp:connect(server.address, port);
end

function network.close ()
    tcp:close()
end

function network.test()
    network.connect(server.tcp)
    --note the newline below
    tcp:send("Connection test\n");
    tcp:settimeout(1)
    local timer = 10
    local done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        if partial == "CONNECTION TEST" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.tcp(msg)
    local msg = msg or ""
    network.connect(server.tcp)
    --note the newline below
    tcp:send(msg.."\n");
    tcp:settimeout(1)
    local timer = 10
    local done = {}

    while timer > 0 do
        local s, status, partial = tcp:receive()
        --print(s, status, partial)
        if partial then
            done = loadstring("return "..partial)()
            timer = 0
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.udp_send(msg)
  local msg = msg or ""
  udp:send(msg)
end

function network.udp_recive()
  local data, msg = udp:receive()
  tmp2 = {}
  if data then
    --print(data)
    tmp2 = loadstring("return "..data)()
  end
  return tmp2
end

return network
