-------------------------------------------------------------------
-- Game includes
-------------------------------------------------------------------

local functions = require("openfun-core.functions")
local network = require("openfun-core.network")
local Player = require("player")
local Sector = require("sector")
local text = require("text")
-------------------------------------------------------------------
-- Variables
-------------------------------------------------------------------

local field_with = 22
local field_height = 16
local tx = 0
local ty = 0
local seed = 10
local width = 0
local height = 0
local network_timer = 0
local network_timer_base = 1/15

local time = 0

tile = {
  width = 32,
  height = 32,
}

image = {
  black = functions.generateBox(tile.width,tile.height,{0,0,0},25),
  blue = functions.generateBox(tile.width,tile.height,{0,0,255}),
  green = functions.generateBox(tile.width,tile.height,{0,255,0}),
  white = functions.generateBox(tile.width,tile.height,{255,255,255}),
  red = functions.generateBox(tile.width,tile.height,{255,0,0}),
  yellow = functions.generateBox(tile.width,tile.height,{255,255,0}),
  azure = functions.generateBox(tile.width,tile.height,{0,255,255})
}

local bg = functions.generateBox(128,32,{50,50,50},25)
local bg2 = functions.generateBox(128,64,{30,30,30},25)

tile[0] =  image.black
tile[1] =  image.green

local players = {}
local shoots = {}
local map = {}

-------------------------------------------------------------------
-- Game itself
-------------------------------------------------------------------

function love.load()
  players = {}
  network.load()
  if not network.test() then
    print("server not fount")
  end
  map = Sector:init(field_with,field_height,seed)
  local x = math.random(map.width-2)+1
  local y = math.random(map.height-2)+1
  local tmp = network.tcp("register ".."venca "..x.." "..y)
    players[1] = Player:init(tmp.id,tmp.name,tmp.color,tmp.x,tmp.y)
end

function love.update(dt)
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  time = time + dt

  if (network_timer < 0) then
    network.udp_send("move "..players[1].id.." "..players[1].x.." "..players[1].y)
    network_timer = network_timer_base
  else
    network_timer = network_timer - dt
  end

  local tmp = network.udp_recive()
  if tmp[1] then
    for k,v in ipairs(tmp[1]) do
      local add = true
      for a,b in ipairs(players) do
        if (v.id == b.id) then
          b.x = v.x
          b.y = v.y
          b.health = v.health
          b.kills = v.kills
          b.deaths = v.deaths
          add = false
        end
      end
      if add then
        players[#players+1] =  Player:init(v.id,v.name,v.color,v.x,v.y)
      end
    end
  end

  if tmp[2] then
    for k,v in ipairs(tmp[2]) do
      for a,b in ipairs(players) do
        if (v.player == b.id) then
          b:shoot(v.dir,v.x,v.y,1)
        end
      end
    end
  end

  for i,v in ipairs(players) do
    if i == 1 then
      v:update(dt)
       local tmp = v:check(dt,map.map,players)
       if tmp then
         network.tcp("dmg "..v.id.." "..tmp.." 1")
       end
      if v.health > 0 then
        v:control(dt,map.map)
      end
    else
      v:update(dt)
      v:check(dt,map.map,players)
    end
  end
end

function love.keypressed(key, scancode)
  local tmp = players[1]:keypressed(key,scancode)
  if tmp then
    network.tcp("shoot "..players[1].id.." "..tmp.x.." "..tmp.y.." "..tmp.dir)
  end
  if scancode == "r" and 1 > players[1].health then
    local x = math.random(map.width-2)+1
    local y = math.random(map.height-2)+1
    players[1]:reset(x,y,5)
    network.tcp("reset "..players[1].id.." "..players[1].x.." "..players[1].y.." "..players[1].health)
  end
end

function love.draw()
  map:draw()

  y = 168
  x = width - 138
  love.graphics.draw(bg,x,10)
  love.graphics.print(text.game_info,x+10,20)
  love.graphics.draw(bg2,x,52)
  love.graphics.print(text.time..functions.round(time)..text.seconds,x+10,62)
  love.graphics.print(text.map_seed..seed,x+10,80)
  love.graphics.print(text.fps..tostring(love.timer.getFPS( )),x+10,98)
  love.graphics.draw(bg,x,y-42)
  love.graphics.print(text.players,x+10,y-32)
  for i,v in ipairs(players) do
    v:draw()
    if v.health > 0 then
      v:gui(x,y)
      y = y + 74
    end
  end
end
