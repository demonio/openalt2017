#include "network.h"
#include <iostream>
#include <cstring>

std::string Network::TCPsend(std::string request){
    sf::Socket::Status status = socket.connect(this->address,this->tcp_port);
    char buffer[1024];
    if (status == sf::Socket::Done) {
        //std::cout << "TCP request " << request << "\n";
         this->socket.send(request.c_str(), request.size()+1);

        // Receive an answer from the server
        std::size_t received = 0;
         this->socket.receive(buffer, sizeof(buffer), received);
         //std::cout << "TCP buffer " << buffer << "\n";
    } else {
        std::cout << "ERROR: Could not connect to server" << "\n";
    }
    socket.disconnect ();
    return std::string(buffer);
}

void Network::TCPthread(){
    std::string test{"Connection test"};
    //std::cout << "The server said: " <<  this->TCPsend(test) << std::endl;

    this->time = sf::seconds(this->timer_base);
    while(true){
            if (!this->tcp_queue.empty()){
                mutex.lock();
                for(std::vector<std::string>::iterator it=this->tcp_queue.begin();
                                                                                    it !=this->tcp_queue.end();){
                         //std::cout << *it << "\n";
                         std::string tmp = this->TCPsend(*it);
                         //std::cout << tmp << "\n";
                         this->tcp_recive.push_back(tmp);
                         it = this->tcp_queue.erase(it);
                }
                mutex.unlock();
            }
            sf::sleep(time);
    }
}

void Network::UDPthread(){
    this->time = sf::seconds(this->timer_base);
    while(true){
            if (!this->udp_send.empty()){
                //std::cout << "Send: " << this->udp_send << std::endl;
                usocket.send(this->udp_send.c_str(), this->udp_send.size() + 1, this->address, this->udp_port);
                this->udp_send = "";
            }
            char buffer[4096];
            std::size_t received = 0;
            sf::IpAddress sender;
            unsigned short port;
            usocket.receive(buffer, sizeof(buffer), received, sender, port);
            if (received > 0){
                //std::cout << "Recived: " << buffer << std::endl;
                this->udp_recive = buffer;
                std::memset(buffer, 0, sizeof buffer);
            }
            sf::sleep(time);
    }
}
