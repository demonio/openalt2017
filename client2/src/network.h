#ifndef NETWORK
#define NETWORK

#include "main.h"

class Network{
    public:
        void TCPthread();
        void UDPthread();
        std::string TCPsend(std::string);
        void set_server(std::string);
        void set_udp(int);
        void set_tcp(int);
        void set_refresh(float);
        std::vector<std::string> tcp_queue;
        std::vector<std::string> tcp_recive;
        std::string udp_send;
        std::string udp_recive;
    private:
        float timer;
        float timer_base{0.1};
        std::string address{"localhost"};
        sf::IpAddress server{"localhost"};
        int udp_port{25001};
        int tcp_port{25000};
        sf::TcpSocket socket;
        sf::UdpSocket usocket;
        sf::Time time;
};

#endif
