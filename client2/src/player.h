#ifndef PLAYER_H
#define PLAYER_H

#include "main.h"
#include "sector.h"

class BaseShoot{
    public:
        //BaseShoot(std::string);
        int id;
        int x;
        int y;
        std::string direction;
        //sf::Color color{255,0,0};
};

class Shoot{
    public:
        Shoot(std::string,int,int,sf::Color);
        Shoot(BaseShoot&,sf::Color);
        ~Shoot(){};
        void draw(sf::RenderWindow&);
        std::pair<int,int> get_position();
        bool update(sf::Time&, Sector&);
        int id;
        int x;
        int y;
    private:
        float act_x;
        float act_y;
        int width{16};
        int height{16};
        int speed{10};
        float timer;
        float timer_base{0.05};
        std::string direction;
        sf::Color color;
        sf::RectangleShape rectangle;
};

class BasePlayer{
    public:
        BasePlayer();
        BasePlayer(std::string);
        int id;
        int x;
        int y;
        int health;
        std::string name;
        int kills;
        int deaths;
        sf::Color color;
};

class Player{
    public:
        Player();
        Player(int,std::string,sf::Color,int,int,int);
        Player(BasePlayer&);
        ~Player(){};

        bool move(int,int,Sector&);
        bool set_position(BasePlayer&);
        void control(sf::Time&, Sector&);
        bool shoot(std::string,int x = 0,int y = 0);
        bool shoot(BaseShoot&);
        void update(sf::Time&, Sector&);
        void draw(sf::RenderWindow&);
        void set_score(BasePlayer&);
        std::pair<int,int> check(std::vector<Player>&);
        bool gui(unsigned int,unsigned int,sf::RenderWindow&,sf::Text&);
        int id;
        int x{0};
        int y{0};
    private:
        float act_x{0};
        float act_y{0};
        int health{5};
        int speed{10};
        int kills{0};
        int deaths{0};
        unsigned int bullets{5};
        float timer{0};
        float timer_base{0.1};
        std::string name{""};
        std::vector<Shoot> shoots;
        sf::Color color;
        sf::RectangleShape rectangle;
        //sf::Font font;
        sf::RectangleShape thubnail;
        sf::RectangleShape bg;
        //sf::Text text;
};

#endif
