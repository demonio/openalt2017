#ifndef OPTIONS_H
#define OPTIONS_H

#include <unistd.h>
#include <iostream>
#include <pqxx/pqxx>
#include <vector>
#include <list>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <getopt.h>
#include <cstdlib>
#include <dirent.h>
#include <stdlib.h>

struct Option {
  char sopt;
  std::string lopt;
  std::string description;
  std::string dopt;
};

class Options {
  public:
    Options(std::vector< std::vector< std::string > > base_args);
    bool get_opts(int argc, char* argv[]);
    std::map < std::string, std::string > args;
    std::string help();
  private:
    std::vector< std::vector< std::string > > base_args;
    void add_lopt(option *,option);
};

std::vector<std::string> split(const std::string& s, char delim);

#endif
