#include "main.h"
#include "player.h"
#include "sector.h"
#include "network.h"
#include "options.h"
#include "lua.h"
#include <cstring>

sf::Mutex mutex;
int tile_width{32};
int tile_height{32};
std::string username;
sf::Color user_color;
int map_width;
int map_height;
int map_seed;
BasePlayer venca;
sf::Font font;
sf::Text text;
Network network;
sf::Thread thread(&Network::TCPthread, &network);
sf::Thread thread2(&Network::UDPthread, &network);

sf::RenderWindow window(sf::VideoMode(800, 600,sf::Style::Default), "OpenAlt 2015");

void signalHandler( int signum ) {
   std::cout << "Interrupt signal (" << signum << ") received.\n";

   char str[128];
   std::memset(str, 0, sizeof str);
   text.setString(str);
   thread.terminate();
   thread2.terminate();
   window.close();

   exit(signum);
}

int main(int argc, char* argv[]){
    // register signal SIGINT and signal handler
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);

    if (!font.loadFromFile("data/font.ttf")){
        std::cerr << "ERROR: Could not load font!\n";
        return EXIT_FAILURE;
    }
    text.setFont(font);
    text.setCharacterSize(12);

    //handle command line arguments
    std::vector< std::vector< std::string > > a = {
      {"n", "username", "Game username", "Demo"},
      {"c", "color", "User color [R,G,B]", "255,0,0"},
      {"s", "seed", "Map seed", "10"},
      {"w", "width", "Map width", "18"},
      {"h", "height", "Map height", "16"},
      {"r", "refresh", "UDP refresh rate", "0.1"},
      {"s", "server", "Server address", "localhost"},
      {"u", "udp", "UDP port", "25001"},
      {"t", "tcp", "TCP port", "25000"},
    };
    Options options(a);
    bool tmp_help = options.get_opts(argc, argv);
        if (!tmp_help)
          return EXIT_SUCCESS;
      username = options.args["username"];
      std::vector<std::string> elc = split(options.args["color"], ',');
      user_color = sf::Color(std::stoi(elc[0]),std::stoi(elc[1]),std::stoi(elc[2]));
      map_width = std::stoi(options.args["width"]);
      map_height = std::stoi(options.args["height"]);
      map_seed = std::stoi(options.args["seed"]);

    sf::Clock clock;
    window.setFramerateLimit(60);
    window.setVerticalSyncEnabled(true);
    sf::View view{window.getDefaultView()};

    Player player(1,"Demo",sf::Color(255,0,0),10,10,5);
    std::vector<Player> enemy;
    Sector sector(map_width,map_height,map_seed);
    window.requestFocus();

    while (window.isOpen()){
        sf::Time dt = clock.getElapsedTime();
        clock.restart();

        if(window.hasFocus()){
            player.update(dt,sector);
            player.control(dt,sector);
        }

        for(auto & i:enemy){
            i.update(dt,sector);
        }

        sf::Event event;
        while (window.pollEvent(event)){
            switch (event.type){
                case sf::Event::Closed:
                     raise(SIGTERM);
                     break;
                 case sf::Event::KeyPressed:
                     switch (event.key.code){
                        case sf::Keyboard::A:
                 		         player.shoot("left");
             		             break;
                         case sf::Keyboard::W:
                  		         player.shoot("up");
              		             break;
                         case sf::Keyboard::S:
                  		         player.shoot("down");
              		             break;
                         case sf::Keyboard::D:
                  		         player.shoot("right");
              		             break;
                        default:
                            break;
                     } break;
                 case sf::Event::Resized:
                     view.setSize(sf::Vector2f(window.getSize()));
                     window.setView(view);
                     break;
                default:
                    break;
            }
        }

        sf::Vector2u dimensions = window.getSize();
        unsigned int width = dimensions.x-138;

        window.clear();
        sector.draw(window);
        player.draw(window);
        for(auto & i:enemy){
            i.draw(window);
        }
        player.gui(width,10,window,text);
        int y{84};
        for(auto & i:enemy){
            if(i.gui(width,y,window,text))
                y += 74;
        }
        window.display();
    }

    return EXIT_SUCCESS;
}
