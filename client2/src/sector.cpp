#include "sector.h"

Sector::Sector(int w,int h,unsigned int seed){
    this->seed = seed;
    this->width = w;
    this->height = h;

    this->rectangle.setSize(sf::Vector2f(32,32));
    this->rectangle.setFillColor(this->white);

    this->tile.resize(w);
    for(auto & i:this->tile){
        i.resize(h);
    }

    for(int x = 0;x<w;x++){
        this->tile[x][0] = true;
        this->tile[x][h-1] = true;
    }

    for(int y = 0;y<h;y++){
        this->tile[0][y] = true;
        this->tile[w-1][y] = true;
    }

    srand(this->seed);
    int max = (rand() % static_cast<int>((w*h*4)/(w+h)));
    for(int i = 1;i<max;i++){
        int x = (rand() % static_cast<int>(w));
        int y = (rand() % static_cast<int>(h));
        this->tile[x][y] = true;
    }

}

bool Sector::check(int x, int y){
    if ( x >= this->width or x < 0) return false;
    if (y >= this->height or y < 0) return false;
    return this->tile[x][y];
}

void Sector::draw(sf::RenderWindow& window){
    for(int x = 0;x<this->width;x++){
        for(int y = 0;y<this->height;y++){
            if (this->check(x,y)) {
                this->rectangle.setPosition((1+x)*32,(1+y)*32);
                window.draw(this->rectangle);
            }
        }
    }
}
