# -*- coding: utf-8 -*-
#!/usr/bin/python

# -----------------------------------------------------------------
# imports
# -----------------------------------------------------------------

import os
import SocketServer
import sys
import argparse
from time import time, sleep
import datetime
import random
from Queue import *
import threading
from functions import *

# -----------------------------------------------------------------
# Definitions
# -----------------------------------------------------------------

commands = {
    "register" : {
        "desc" : "Register player",
        "command" : register,
        "params" : "Name x y",
    },
    "move" : {
        "desc" : "move player",
        "command" : move,
        "params" : "id x y",
    },
    "shoot" : {
        "desc" : "shoot player",
        "command" : do_shoot,
        "params" : "id x y dir",
    },
    "dmg" : {
        "desc" : "dmg player",
        "command" : dmg,
        "params" : "shooter target dmg",
    },
"reset": {
    "desc" : "Reset player position and health",
    "command" : reset,
    "params" : "id x, y, health",
    }
}

arguments = {
    "address" : {
        "short" : "a",
        "desc" : "Server address",
        "required" : False,
        "default" : "localhost"
    },
    "udp" : {
        "short" : "u",
        "desc" : "UDP port",
        "required" : False,
        "default" : 25001
    },
    "tcp" : {
        "short" : "t",
        "desc" : "TCP port",
        "required" : False,
        "default" : 25000
    },
}

# -----------------------------------------------------------------
# Main
# -----------------------------------------------------------------

if __name__ == '__main__':
    ## Set char set ##
    reload(sys)
    sys.setdefaultencoding('utf-8')

    parser = argparse.ArgumentParser(
        description='Openalt game server')
    for k,v in arguments.iteritems():
        parser.add_argument("-" + v["short"], "--" + k,
            help=v["desc"], required=v["required"], default = v["default"])
    args = parser.parse_args()

    # TODO
    address = (args.address, args.tcp)
    address2 = (args.address, args.udp)
    server = SocketServer.TCPServer(address, TCPHandler)
    server2 = SocketServer.UDPServer(address2, UDPHandler)

    #TCP thread
    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True)  # don't hang on exit
    t.start()

    #UDP thread
    t2 = threading.Thread(target=server2.serve_forever)
    t2.setDaemon(True)
    t2.start()

    #Updater thread
    t3 = threading.Thread(target=updater)
    t3.setDaemon(True)
    t3.start()

    set_commands(commands)

    while 1:
        try:
            # DO THINGS
            command = raw_input(">")
            if command.lower() == "exit":
                print("Exiting")
                server.socket.close()
                server2.socket.close()
                sys.exit()
            else:
                print(use(command))
        except KeyboardInterrupt:
            # quit
            server.socket.close()
            server2.socket.close()
            sys.exit()
