import os
import SocketServer
import sys
import argparse
from time import time, sleep
import datetime
import random
from Queue import *
import threading
import time

commands = {}
clients = []
shoots = []
shoots_total = 0

#Change python dict to lua table
def python_to_lua(tmp):
    typed_list = {}
    for key, value in tmp.items():
        if type(value) == str:
            typed_list[key] = '"'+value+'"'
        elif type(value) == bool:
            typed_list[key] = str(value).lower()
        elif type(value) == list:
            typed_list[key] = str(value).replace("[", "{").replace("]", "}")
        else:
            typed_list[key] = str(value)
    return "{{{}}}".format(", ".join("{} = {}".format(key, typed_list[key]) for key in typed_list))

#get data from tcp communication and send it to use function
class TCPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request.recv(1024).strip()
        #print(data)
        if data.find("Connection test") > -1:
            self.request.send(data.upper())
        else:
            self.request.send(use(data))
        return

#get data from udp communication and send it to use function
class UDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        #print(data)
        socket = self.request[1]
        value = use(data)
        socket.sendto(value, self.client_address)

#if you need to update something over time this function is for you
def updater():
    global shoots, clients
    while True:
        startTime = time.time()
        for i in clients:
            if i.timer > 0:
                i.timer -= 0.1
            else:
                clients.remove(i)
        for i in shoots:
            if i.timer > 0:
                i.timer -= 0.1
            else:
                shoots.remove(i)
        endTime = time.time() - startTime
        sleep(0.1 - endTime)

# -----------------------------------------------------------------
#  Classes
# -----------------------------------------------------------------

class shoot():

    def __init__(self,values):
        self.player = int(values[0])
        self.id = 0
        self.x = int(values[1])
        self.y = int(values[2])
        self.dir = values[3]
        self.timer = 0.2

    def get_info(self):
            tmp = {}
            tmp["id"] = self.id
            tmp["player"] = self.player
            tmp["x"] = self.x
            tmp["y"] = self.y
            tmp["dir"] = self.dir

            return python_to_lua(tmp)

class client():

    def __init__(self,values):
        self.name = values[0]
        self.x = int(values[1])
        self.y = int(values[2])
        self.color = ""
        self.id = 0
        self.health = 5
        self.timer = 1
        self.shoots = {}
        self.deaths = 0
        self.kills = 0
        print("New player "+self.name)

    def get_info(self):
            tmp = {}
            tmp["id"] = self.id
            tmp["name"] = self.name
            tmp["x"] = self.x
            tmp["y"] = self.y
            tmp["kills"] = self.kills
            tmp["deaths"] = self.deaths
            tmp["color"] = self.color
            tmp["health"] = self.health

            return python_to_lua(tmp)

# -----------------------------------------------------------------
# Game server commands
# -----------------------------------------------------------------

def register(values):
    clients.append(client(values))
    index = len(clients)-1
    tmp_color = random.sample(xrange(20,225),3)
    clients[index].color = tmp_color
    clients[index].id = index
    return clients[index].get_info()

def reset(values):
    clients[int(values[0])].x = int(values[1])
    clients[int(values[0])].y = int(values[2])
    clients[int(values[0])].health = int(values[3])
    return "DONE"

def do_shoot(values):
    shoots.append(shoot(values))
    index = len(shoots)-1
    shoots[index].id = index
    return "DONE"

def move(values):
    clients[int(values[0])].x = int(values[1])
    clients[int(values[0])].y = int(values[2])
    clients[int(values[0])].timer = 1
    tmp = "{{"
    for i in clients:
        tmp += i.get_info()+","
    tmp += "},{"
    for i in shoots:
        tmp += i.get_info()+","
    return tmp+"}}--"

def dmg(values):
    clients[int(values[1])].health -= int(values[2])
    if clients[int(values[1])].health < 1:
        clients[int(values[0])].kills += 1
        clients[int(values[1])].deaths += 1
    return "DONE"

# -----------------------------------------------------------------
# General functions
# -----------------------------------------------------------------

#check command and print help for them
def usage():
    tmp = ""
    for i,v in commands.iteritems():
        tmp += "Command: " + i.ljust(20) + "\t Params: " + v["params"].ljust(20) + "\t Description: " +  v["desc"] + "\n"
    return tmp

#if there is a command give it params and execute it
def use(data):
    if not data:
        return ""
    tmp = data.split()
    command = tmp[0]
    del tmp[0]
    #print(command, tmp)
    if command.lower() == "help":
        return usage()
    try:
        return commands[command.lower()]["command"](tmp)
    except:
        return "Command not found"

#forward commands
def set_commands(data):
    global commands
    #print(data)
    commands = data
