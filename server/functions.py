import os
import SocketServer
import sys
import argparse
from time import time, sleep
import datetime
import random
from Queue import *
import threading
import time

commands = {}
clients = []
shoots = []
shoots_total = 0

#Change python dict to lua table
def python_to_lua(tmp):
    typed_list = {}
    for key, value in tmp.items():
        if type(value) == str:
            typed_list[key] = '"'+value+'"'
        elif type(value) == bool:
            typed_list[key] = str(value).lower()
        elif type(value) == list:
            typed_list[key] = str(value).replace("[", "{").replace("]", "}")
        else:
            typed_list[key] = str(value)
    return "{{{}}}".format(", ".join("{} = {}".format(key, typed_list[key]) for key in typed_list))

#get data from tcp communication and send it to use function
class TCPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request.recv(1024).strip()
        #print(data)
        if data.find("Connection test") > -1:
            self.request.send(data.upper())
        else:
            self.request.send(use(data))
        return

#get data from udp communication and send it to use function
class UDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        #print(data)
        socket = self.request[1]
        value = use(data)
        socket.sendto(value, self.client_address)

#if you need to update something over time this function is for you
def updater():
    global shoots, clients
    while True:
        startTime = time.time()

        endTime = time.time() - startTime
        sleep(0.1 - endTime)

# -----------------------------------------------------------------
#  Classes
# -----------------------------------------------------------------


# -----------------------------------------------------------------
# Game server commands
# -----------------------------------------------------------------



# -----------------------------------------------------------------
# General functions
# -----------------------------------------------------------------

#check command and print help for them
def usage():
    tmp = ""
    for i,v in commands.iteritems():
        tmp += "Command: " + i.ljust(20) + "\t Params: " + v["params"].ljust(20) + "\t Description: " +  v["desc"] + "\n"
    return tmp

#if there is a command give it params and execute it
def use(data):
    if not data:
        return ""
    tmp = data.split()
    command = tmp[0]
    del tmp[0]
    #print(command, tmp)
    if command.lower() == "help":
        return usage()
    try:
        return commands[command.lower()]["command"](tmp)
    except:
        return "Command not found"

#forward commands
def set_commands(data):
    global commands
    #print(data)
    commands = data
