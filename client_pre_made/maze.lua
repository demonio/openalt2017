local function getopt( arg, options )
  local tab = {}
  for k, v in ipairs(arg) do
    if string.sub( v, 1, 2) == "--" then
      local x = string.find( v, "=", 1, true )
      if x then tab[ string.sub( v, 3, x-1 ) ] = string.sub( v, x+1 )
      else      tab[ string.sub( v, 3 ) ] = true
      end
    elseif string.sub( v, 1, 1 ) == "-" then
      local y = 2
      local l = string.len(v)
      local jopt
      while ( y <= l ) do
        jopt = string.sub( v, y, y )
        if string.find( options, jopt, 1, true ) then
          if y < l then
            tab[ jopt ] = string.sub( v, y+1 )
            y = l
          else
            tab[ jopt ] = arg[ k + 1 ]
          end
        else
          tab[ jopt ] = true
        end
        y = y + 1
      end
    end
  end
  return tab
end

local opts = getopt( arg, "ab" )

if opts["h"] or opts["help"] then
	print("USAGE: lua map.lua --wall='#' --seed=1 --width=64 --height=64")
	os.exit()
end

local width = opts["width"] or 32
local height = opts["height"] or 32
local wall = opts["wall"] or "#"
local seed = opts["seed"] or os.time()

math.randomseed(seed)

local room_width = math.floor(math.random(width/8))
local room_height = math.floor(math.random(height/8))

local room_count = math.floor(math.random((width+height)/(room_width+room_height)))+4

print(width,height,room_width,room_height,room_count)

maze = {}

for x=1,width do
  maze[x] = {}
  for y=1,height do
    maze[x][y] = false
  end
end

print(#maze,#maze[1])

local door = {{0,0}}

for i=1,room_count do
  local rw = math.floor(math.random(room_width))+4
  local rh = math.floor(math.random(room_height))+4
  local rx = math.floor(math.random(width-rw-4))+2
  local ry = math.floor(math.random(height-rh-4))+2
  local rd = math.floor(math.random((rw*2)+(rh*2)-10))+2
  for a=1,3 do
    local mr = true
    local md = true
    for x=rx-1,rx+rw+1 do
      for y=ry-1,ry+rh+1 do
        if maze[x][y] then mr = false end
      end
    end
    if mr then
    for x=rx,rx+rw do
      maze[x][ry] = (rd ~= 0 and true or false)
      if not maze[x][ry] then rd = rd - 1; door[#door+1] = {x,ry} end
      rd = ((x ~= rx or x == rx-rw) and rd - 1 or rd)
      maze[x][ry+rh] = (rd ~= 0 and true or false)
      if not maze[x][ry+rh] then rd = rd - 1; door[#door+1] = {x,ry-rh} end
      rd = ((x ~= rx or x == rx-rw) and rd - 1 or rd)
    end
    for y=ry,ry+rh do
      maze[rx][y] = (rd ~= 0 and true or false)
      if not maze[rx][y] then rd = rd - 1; door[#door+1] = {rx,y} end
      rd = ((y ~= ry or y == ry-rh) and rd - 1 or rd)
      maze[rw+rx][y] = (rd ~= 0 and true or false)
      if not maze[rw+rx][y] then rd = rd - 1; door[#door+1] = {rw+rx,y} end
      rd = ((y ~= ry or y == ry-rh) and rd - 1 or rd)
    end
  end
    if mr then
      break
    end
  end
end

for x=1,width do
  maze[x][1] = true
  maze[x][#maze[1]] = true
end

for y=1,height do
  maze[1][y] = true
  maze[#maze][y]= true
end


for y=1,height do
  for x=1,width do
    if(maze[x][y]) then
      io.write("#")
    else
      io.write(" ")
    end
  end
  io.write("\n")
end
