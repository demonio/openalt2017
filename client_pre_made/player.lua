local functions = require("openfun-core/functions")
local text = require("text")
require("class")
local dir = {
  left = {x=-1,y=0},
  right = {x=1,y=0},
  up = {x=0,y=-1},
  down = {x=0,y=1}
}

local player = class()

function player:init(id, name,color,x,y,health)
  local id = id or 1
  local name = name or "Demo"
  local color = color or {255,0,0}
  local x = x or 10
  local y = y or 10
  local health = health or 5
  local tmp = {}
  setmetatable(tmp,player)

  tmp.bg = functions.generateBox(128,64,{30,30,30},25)
  tmp.id = id
  tmp.x = x
  tmp.y = y
  tmp.act_x = 0
  tmp.act_y = 0
  tmp.health = health
  tmp.bullets = 5
  tmp.speed = 10
  tmp.kills = 0
  tmp.deaths = 0
  tmp.timer = 0
  tmp.timer_base = 1/10
  tmp.color = color
  tmp.name = name
  tmp.shoots = {}
  tmp.img = functions.generateBox(tile.width,tile.height,tmp.color)

  return tmp
end

function player:move(map,x,y)
  if not map[self.x+x] then
    return
  end
  if not map[self.x+x][self.y+y] then
    return
  end
  if map[self.x+x][self.y+y]["tile"] == 0 then
    self.x = self.x + x
    self.y = self.y + y
  end
end

function player:update(dt,tile_width,tile_height)
  local tile_width = tile_width or 32
  local tile_height = tile_height or 32
  self.act_y = self.act_y - ((self.act_y - self.y*tile_height) * self.speed * dt)
	self.act_x = self.act_x - ((self.act_x - self.x*tile_width) * self.speed * dt)
end

function player:check(dt,map,players,tile_width,tile_height)

  local tile_width = tile_width or 32
  local tile_height = tile_height or 32
  for i,v in pairs(self.shoots) do
    if v.act_x == 0 and v.act_y == 0 then
      v.act_x = v.x*tile_width
      v.act_y = v.y*tile_height
    end
    if v.timer <= 0 then
      if not map[v.x+dir[v.dir].x] then
        table.remove(self.shoots,i)
        self.bullets = self.bullets + 1
        break
      end
      if not map[v.x+dir[v.dir].x][v.y+dir[v.dir].y] then
        table.remove(self.shoots,i)
        self.bullets = self.bullets + 1
        break
      end
      for k,g in ipairs(players) do
        if g.x == v.x and g.y == v.y and g.name ~= self.name and g.health > 0 then
          table.remove(self.shoots,i)
          self.bullets = self.bullets + 1
          return g.id
        end
      end
      if map[v.x+dir[v.dir].x][v.y+dir[v.dir].y]["tile"] == 0 then
        v.x = v.x + dir[v.dir].x
        v.y = v.y + dir[v.dir].y
      else
        table.remove(self.shoots,i)
        self.bullets = self.bullets + 1
        break
      end
      v.timer = v.timer_base
    else
      v.timer = v.timer - dt
    end
    v.act_y = v.act_y - ((v.act_y - v.y*tile_height) * v.speed * dt)
    v.act_x = v.act_x - ((v.act_x - v.x*tile_width) * v.speed * dt)
  end
end

function player:control(dt,map)
    if self.timer <= 0 then
      if love.keyboard.isDown('up') then 	self:move(map,0,-1) end
      if love.keyboard.isDown('down') then 	self:move(map,0,1) end
  		if love.keyboard.isDown('left') then 	self:move(map,-1,0) end
  		if love.keyboard.isDown('right') then 	self:move(map,1,0) end
      self.timer = self.timer_base
    else
      self.timer = self.timer - dt
    end
end

function player:set_position(x,y)
  self.x = x
  self.y = y
end

function player:reset(x,y,health)
  local x = x or 10
  local y = y or 10
  local health = health or 5

  self.x = x
  self.y = y
  self.health = health
end

function player:shoot(direction,x,y,id)
  local tmp_bullets = self.bullets or 1
  if tmp_bullets > 0 and self.health > 0 then
    local tmp = {}
    tmp.x = x or self.x
    tmp.y = y or self.y
    tmp.id = id or 0
    tmp.act_x = 0
    tmp.act_y = 0
    tmp.speed = 10
    tmp.timer = 0
    tmp.timer_base = 1/20
    tmp.dir = direction
    table.insert(self.shoots,tmp)
    self.bullets = self.bullets - 1
    return tmp
  end
  return nil
end

function player:keypressed(key, scancode)
  if scancode == "w" then return self:shoot("up") end
  if scancode == "a" then return self:shoot("left") end
  if scancode == "s" then return self:shoot("down") end
  if scancode == "d" then return self:shoot("right") end
end

function player:draw(tx,ty,tile_width,tile_height,scale,rotation)
  local tx = tx or 0
  local ty = ty or 0
  local tile_width = tile_width or 32
  local tile_height = tile_height or 32
  local scale = scale or 1
  local rotation = rotation or 0


  if self.health > 0 then
    love.graphics.draw(self.img,self.act_x,self.act_y)
    for i,v in pairs(self.shoots) do
      love.graphics.draw(self.img,v.act_x,v.act_y,rotation,scale/2,scale/2,-tile_width/2,-tile_height/2)
    end
  end
end

function player:gui(x,y)
  local x = x or 0
  local y =y or 0

  love.graphics.draw(self.bg,x,y)
  love.graphics.draw(self.img,x+10,y+10,0,0.5,0.5)
  love.graphics.print(self.name,x+32,y+10)
  love.graphics.print(text.lives..self.health.." | "..text.shoots..self.bullets,x+10,y+28)
  love.graphics.print(text.kills..self.kills.." | "..text.deaths..self.deaths,x+10,y+44)
end

return player
