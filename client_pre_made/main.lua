-------------------------------------------------------------------
-- Game includes
-------------------------------------------------------------------

local functions = require("openfun-core.functions")
local Player = require("player")
local Sector = require("sector")
local network = require("openfun-core.network")
local text = require("text")
-------------------------------------------------------------------
-- Variables
-------------------------------------------------------------------

local field_with = 18
local field_height = 16
local tx = 0
local ty = 0
local seed = 10
local width = 0
local height = 0
local network_timer = 0
local network_timer_base = 1/15

local time = 0

tile = {
  width = 32,
  height = 32,
}

image = {
  black = functions.generateBox(tile.width,tile.height,{0,0,0},25),
  blue = functions.generateBox(tile.width,tile.height,{0,0,255}),
  green = functions.generateBox(tile.width,tile.height,{0,255,0}),
  white = functions.generateBox(tile.width,tile.height,{255,255,255}),
  red = functions.generateBox(tile.width,tile.height,{255,0,0}),
  yellow = functions.generateBox(tile.width,tile.height,{255,255,0}),
  azure = functions.generateBox(tile.width,tile.height,{0,255,255})
}

local bg = functions.generateBox(128,32,{50,50,50},25)
local bg2 = functions.generateBox(128,64,{30,30,30},25)

tile[0] =  image.black
tile[1] =  image.green

local players = {}
local shoots = {}
local map = {}

-------------------------------------------------------------------
-- Game itself
-------------------------------------------------------------------

function love.load()
  players = {}
  network.load()
  if not network.test() then
    print("ERROR: Server is not avaible")
    os.exit()
  end
  map = Sector:init(field_with,field_height,seed)
    --players[1] = Player:init("Venca","red",
      --math.random(map.width-2)+1,math.random(map.height-2)+1)
  math.randomseed(os.time())
  local x = math.random(map.width-2)+1
  local y = math.random(map.height-2)+1
  --print(math.random(1000000))
  local tmp = network.tcp("register " .. " "..math.random(1000000).." " .. " " .. x .. " ".. y)
  --print(tmp.id, tmp.name, tmp.color, tmp.x,tmp.y)
  players[1] = Player:init(tmp.id, tmp.name, tmp.color, tmp.x,tmp.y,tmp.health)
end

function love.update(dt)
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  time = time + dt

  local data = network.udp_recive()
  local total_players = #players
  if data.player then
    for j,k in ipairs(data.player) do
      total_players = #players
      local ok = false
      for i,v in ipairs(players) do
        if k.id == v.id then
          ok = true
          v.kills = k.kills
          v.deaths = k.deaths
          v.health = k.health
          if i > 1 then
            v.x = k.x
            v.y = k.y
            if data.shoots then
              for a,b in ipairs(data.shoots) do
                if b.player == v.id  then
                  local sok = false
                  for c, d in ipairs(v.shoots) do
                    if b.id == d.id then
                      sok = true
                    end
                  end
                  if not sok then
                    players[i]:shoot(b.dir,b.x,b.y,b.id)
                  end
                end
              end
            end
          end
        end
      end
      if not ok then
        players[total_players+1] =  Player:init(k.id,k.name,k.color,k.x,k.y,k.health)
      end
    end
  end

  if network_timer <= 0 then
    network.udp_send("move "..players[1].id.." "..players[1].x.." "..players[1].y)
    network_timer = network_timer_base
  else
    network_timer = network_timer - dt
  end

  for i,v in ipairs(players) do
    if i == 1 then
      v:update(dt)
      local test = v:check(dt,map.map,players)
      if test then
        network.tcp("dmg "..players[1].id .." "..test.." 1")
      end
      if v.health > 0 then
        v:control(dt,map.map)
      end
    else
      v:update(dt)
      v:check(dt,map.map,players)
    end
  end
end

function love.keypressed(key, scancode)
  local tmp = players[1]:keypressed(key,scancode)
  if tmp then
    network.tcp("shoot "..players[1].id.." "..tmp.x.." "..tmp.y.." "..tmp.dir)
  end
  if scancode == "r" and 1 > players[1].health then
    local x = math.random(map.width-2)+1
    local y = math.random(map.height-2)+1
    players[1]:reset(x,y,5)
    network.tcp("reset "..players[1].id.." "..players[1].x.." "..players[1].y.." "..players[1].health)
  end
end

function love.draw()
  map:draw()

  y = 168
  x = width - 138
  love.graphics.draw(bg,x,10)
  love.graphics.print(text.game_info,x+10,20)
  love.graphics.draw(bg2,x,52)
  love.graphics.print(text.time..functions.round(time)..text.seconds,x+10,62)
  love.graphics.print(text.map_seed..seed,x+10,80)
  love.graphics.print(text.fps..tostring(love.timer.getFPS( )),x+10,98)
  love.graphics.draw(bg,x,y-42)
  love.graphics.print(text.players,x+10,y-32)
  for i,v in ipairs(players) do
    v:draw()
    if v.health > 0 then
      v:gui(x,y)
      y = y + 74
    end
  end
end
