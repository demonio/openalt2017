#include "main.h"
#include "player.h"
#include "sector.h"
#include "network.h"
#include "options.h"
#include "lua.h"
#include <cstring>

sf::Mutex mutex;
int tile_width{32};
int tile_height{32};
std::string username;
sf::Color user_color;
int map_width;
int map_height;
int map_seed;
BasePlayer venca;
sf::Font font;
sf::Text text;
Network network;
sf::Thread thread(&Network::TCPthread, &network);
sf::Thread thread2(&Network::UDPthread, &network);

sf::RenderWindow window(sf::VideoMode(800, 600,sf::Style::Default), "OpenAlt 2015");

void signalHandler( int signum ) {
   std::cout << "Interrupt signal (" << signum << ") received.\n";

   char str[128];
   std::memset(str, 0, sizeof str);
   text.setString(str);
   thread.terminate();
   thread2.terminate();
   window.close();

   exit(signum);
}

int main(int argc, char* argv[]){
    // register signal SIGINT and signal handler
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);

    if (!font.loadFromFile("data/font.ttf")){
        std::cerr << "ERROR: Could not load font!\n";
        return EXIT_FAILURE;
    }
    text.setFont(font);
    text.setCharacterSize(12);

    //handle command line arguments
    std::vector< std::vector< std::string > > a = {
      {"n", "username", "Game username", "Demo"},
      {"c", "color", "User color [R,G,B]", "255,0,0"},
      {"s", "seed", "Map seed", "10"},
      {"w", "width", "Map width", "18"},
      {"h", "height", "Map height", "16"},
      {"r", "refresh", "UDP refresh rate", "0.1"},
      {"s", "server", "Server address", "localhost"},
      {"u", "udp", "UDP port", "25001"},
      {"t", "tcp", "TCP port", "25000"},
    };
    Options options(a);
    bool tmp_help = options.get_opts(argc, argv);
    if (!tmp_help)
      return EXIT_SUCCESS;
      username = options.args["username"];
      std::vector<std::string> elc = split(options.args["color"], ',');
      user_color = sf::Color(std::stoi(elc[0]),std::stoi(elc[1]),std::stoi(elc[2]));
      map_width = std::stoi(options.args["width"]);
      map_height = std::stoi(options.args["height"]);
      map_seed = std::stoi(options.args["seed"]);

    sf::Clock clock;
    window.setFramerateLimit(60);
    window.setVerticalSyncEnabled(true);
    sf::View view{window.getDefaultView()};

    //Start threads
    thread.launch();

    std::string tmp_send;
    tmp_send = "register "+username+" 10 10\n";
    network.tcp_queue.push_back(tmp_send);

    while(true){
        std::cout << "Waiting for player info ..." << "\n";
        if(!network.tcp_recive.empty()){
            std::string luas = network.tcp_recive[0];
            std::cout << luas << "\n";
            std::string tp = "player ="+luas;
            venca = BasePlayer(tp);
            break;
        } else
            usleep(1000000);
    }

    //Player player(1,"Demo",sf::Color(255,0,0),10,10,5);
    Player player(venca);
    std::vector<Player> enemy;
    std::vector<BasePlayer> enemy2;
    /*try{
            player=Player(venca);
    } catch (...){
        std::cout << "Could not create player \n";
        player=Player(1,"Demo",sf::Color(255,0,0),10,10,5);
    }*/
    Sector sector(map_width,map_height,map_seed);
    window.requestFocus();

    thread2.launch();

    while (window.isOpen()){
        sf::Time dt = clock.getElapsedTime();
        clock.restart();

        //if(window.hasFocus()){
            player.update(dt,sector);
            player.control(dt,sector);
        //}
        for(auto & i:enemy){
            i.update(dt,sector);
        }

        auto dmg_check =  player.check(enemy);
        if(dmg_check.second > -1){
            std::string space{" "};
            tmp_send = "dmg "+std::to_string(dmg_check.first)+space+std::to_string(dmg_check.second)+" 1 \n";
            network.tcp_queue.push_back(tmp_send);
        }

        if(!player.shoot_send.empty()){
            network.tcp_queue.push_back(player.shoot_send);
            player.shoot_send = "";
        }
        if(!player.move_send.empty()){
            network.udp_send = player.move_send;
            player.move_send = "";
        }

        if(!network.udp_recive.empty()){
            LuaAdapter lua;
            std::string tp = "game ="+network.udp_recive;
            //std::cout << tp << "\n";
            //lua.Debug();
            lua.DoString(tp.c_str());
            if (lua.OpenTable("game")) {
                if (lua.OpenNestedTable("player")) {
                        const unsigned short int length{lua.GetTableLength()};
                        for (unsigned int i = 1; i <= length; i++) {
                            //std::cout << "Getting player info " << i << "\n";
                            std::string ts;
                            if(lua.OpenNestedTable(i)){
                                BasePlayer tpa;
                                lua.GetField("id", tpa.id);
                                lua.GetField("x", tpa.x);
                                lua.GetField("y", tpa.y);
                                lua.GetField("name", tpa.name);
                                lua.GetField("health", tpa.health);
                                lua.GetField("kills", tpa.kills);
                                lua.GetField("deaths", tpa.deaths);
                                if (lua.OpenNestedTable("color")) {
                                    int r,g,b;
                                    lua.GetField(1, r);
                                    lua.GetField(2, g);
                                    lua.GetField(3, b);
                                    tpa.color = sf::Color(r,g,b);
                                    lua.CloseTable();
                                }
                                bool found{false};
                                if(tpa.id == player.id){
                                    player.set_score(tpa);
                                    found = true;
                                }
                                for(auto & i:enemy){
                                    if (i.id == tpa.id){
                                        found = true;
                                        i.set_position(tpa);
                                    }
                                }
                                if(!found){
                                    try{
                                        //std::cout << "Inserting player " << tpa.name<< "\n";
                                        enemy.push_back(Player(tpa));
                                    } catch (...){
                                        std::cout << "Problem with inserting player \n";
                                    }
                                }
                                lua.CloseTable();
                            }
                        }
                        lua.CloseTable();
                }
                if (lua.OpenNestedTable("shoots")) {
                    const unsigned short int length{lua.GetTableLength()};
                    for (unsigned int i = 1; i <= length; i++) {
                        if(lua.OpenNestedTable(i)){
                            BaseShoot ts;
                            int tpb;
                            lua.GetField("id", ts.id);
                            lua.GetField("x", ts.x);
                            lua.GetField("y", ts.y);
                            lua.GetField("dir", ts.direction);
                            lua.GetField("player", tpb);
                            for(auto & i:enemy){
                                if (i.id == tpb){
                                    i.shoot(ts);
                                }
                            }
                        }
                    }
                }
            }
            network.udp_recive = "";
            //return EXIT_SUCCESS;
        }

        sf::Event event;
        while (window.pollEvent(event)){
            switch (event.type){
                case sf::Event::Closed:
                     raise(SIGTERM);
                     break;
                 case sf::Event::KeyPressed:
                     switch (event.key.code){
                        case sf::Keyboard::A:
                 		         player.shoot("left");
             		             break;
                         case sf::Keyboard::W:
                  		         player.shoot("up");
              		             break;
                         case sf::Keyboard::S:
                  		         player.shoot("down");
              		             break;
                         case sf::Keyboard::D:
                  		         player.shoot("right");
              		             break;
                        default:
                            break;
                     } break;
                 case sf::Event::Resized:
                     view.setSize(sf::Vector2f(window.getSize()));
                     window.setView(view);
                     break;
                default:
                    break;
            }
        }

        sf::Vector2u dimensions = window.getSize();
        unsigned int width = dimensions.x-138;

        window.clear();
        sector.draw(window);
        player.draw(window);
        for(auto & i:enemy){
            i.draw(window);
        }
        player.gui(width,10,window,text);
        int y{84};
        for(auto & i:enemy){
            if(i.gui(width,y,window,text))
                y += 74;
        }
        window.display();
    }

    return EXIT_SUCCESS;
}
