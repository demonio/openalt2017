#include "options.h"

Options::Options(std::vector< std::vector< std::string > > base_args){
  for(auto & i : base_args) {
    this->args[i[1]] = i[3];
    //std::cout <<i[1] << "  -  " << i[3] << std::endl;
  }
  this->base_args = base_args;
}

void Options::add_lopt(option *p,option a){
  int tmp_max = (int) sizeof(p);
  p[tmp_max] = a;
}

bool Options::get_opts(int argc, char* argv[]) {
  std::string shelp = "-h";
  std::string lhelp = "--help";
  for (int i = 1; i < argc;i++) {
    //std::cout << argv[i] << std::endl;
    for (auto & item: base_args){
      //std::cout << argv[i] << "  -  " << item[0] << std::endl;
      if (argv[i] == "-"+item[0] || argv[i] == "--"+item[1]){
        args[item[1]] = argv[i+1];
      } else if (argv[i] == shelp || argv[i] == lhelp) {
        std::cout << this->help() << std::endl;
        return false;
      }
    }
  }
  return true;
}


std::string Options::help(){
  std::string tmp_help = "Commands help: \n";
  for (auto & item: base_args){
    tmp_help += "\t -" + item[0] + " --" + item[1] + "\t " + item[2] + "\n";
  }
  return tmp_help;
}

std::vector<std::string> split(const std::string& s, char delim)
{
    bool                        extend = false;
    std::vector<std::string>    elems;
    std::stringstream           linestream(s);
    std::string                 value;

    if (s.back() == delim) {
        extend = true;
    }
    while(getline(linestream,value,delim))
    {
        elems.push_back(value);
    }
    if (extend) {
        elems.push_back("");
    }
    return elems;
}
