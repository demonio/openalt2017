/**
*
*
*   @author    Václav Muller
*   @version   1.0
*
*/
#ifndef MAIN_H
#define MAIN_H

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>
#include <csignal>

extern sf::Mutex mutex;

#endif
