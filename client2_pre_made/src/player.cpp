#include "player.h"
#include "lua.h"

//-----------------------------
//Base Player
//-----------------------------

BasePlayer::BasePlayer(){
}

BasePlayer::BasePlayer(std::string tp){
    LuaAdapter lua;
    //lua.Debug();
      lua.DoString(tp.c_str());
      if (lua.OpenTable("player")) {
          lua.GetField("id", this->id);
          lua.GetField("x", this->x);
          lua.GetField("y", this->y);
          lua.GetField("name", this->name);
          lua.GetField("health", this->health);
          lua.GetField("kills", this->kills);
          lua.GetField("deaths", this->deaths);
         if (lua.OpenNestedTable("color")) {
             int r,g,b;
             lua.GetField(1, r);
             lua.GetField(2, g);
             lua.GetField(3, b);
             this->color = sf::Color(r,g,b);
             lua.CloseTable();
         }
         lua.CloseTable();
    }
}

//-----------------------------
//Base Player
//-----------------------------

std::map<std::string,std::pair<int,int>> direction_base{
    {"left",{-1,0}},
    {"right",{1,0}},
    {"up",{0,-1}},
    {"down",{0,1}}
};

Shoot::Shoot(std::string dir,int x,int y,sf::Color color){
    this->x = x; this->y = y;
    this->act_x = x*32; this->act_y = y*32;
    this->color = color;
    this->direction = dir;
    this->rectangle.setSize(sf::Vector2f(this->width,this->height));
    this->rectangle.setFillColor(this->color);
    this->rectangle.setPosition(this->act_x,this->act_y);
}

Shoot::Shoot(BaseShoot& bs,sf::Color color){
    this->id = bs.id;
    this->x = bs.x; this->y = bs.y;
    this->act_x = bs.x*32; this->act_y = bs.y*32;
    this->color = color;
    this->direction = bs.direction;
    this->rectangle.setSize(sf::Vector2f(this->width,this->height));
    this->rectangle.setFillColor(this->color);
    this->rectangle.setPosition(this->act_x,this->act_y);
}

bool Shoot::update(sf::Time & dt, Sector& sector){
    if (this->timer < 0){
        this->x += direction_base[this->direction].first;
        this->y += direction_base[this->direction].second;
        this->timer = this->timer_base;
        if(sector.check(this->x-1,this->y-1)) return true;
    }else{
        this->timer -= dt.asSeconds();
    }
    int tile_height{32};
    int tile_width{32};
    this->act_y = this->act_y - ((this->act_y - this->y*tile_height)*this->speed*dt.asSeconds());
    this->act_x = this->act_x - ((this->act_x - this->x*tile_width)*this->speed*dt.asSeconds());
    this->rectangle.setPosition(this->act_x+(tile_width/4),this->act_y+(tile_height/4));
    return false;
}

void Shoot::draw(sf::RenderWindow& window){
    window.draw(this->rectangle);
}

Player::Player(){
}
Player::Player(int id,
    std::string name,
    sf::Color color,
    int x,int y,int health) {
    this->id = id;
    this->name = name;
    this->color = color;
    this->health = health;
    this->x = x; this->y = y;
    this->rectangle.setSize(sf::Vector2f(32,32));
    this->rectangle.setFillColor(this->color);
    this->rectangle.setPosition(this->act_x,this->act_y);
    this->bg.setSize(sf::Vector2f(128,64));
    this->bg.setFillColor(sf::Color(30,30,30));
    //this->font.loadFromFile("data/font.ttf");
    //this->text.setFont(font);
    //this->text.setCharacterSize(12);
    this->thubnail.setSize(sf::Vector2f(16,16));
    this->thubnail.setFillColor(this->color);
}

Player::Player(BasePlayer& bs){
    this->id = bs.id;
    this->name = bs.name;
    this->color = bs.color;
    this->health = bs.health;
    this->x = bs.x; this->y = bs.y;
    this->rectangle.setSize(sf::Vector2f(32,32));
    this->rectangle.setFillColor(this->color);
    this->rectangle.setPosition(this->act_x,this->act_y);
    this->bg.setSize(sf::Vector2f(128,64));
    this->bg.setFillColor(sf::Color(30,30,30));
    //this->font.loadFromFile("data/font.ttf");
    //this->text.setFont(font);
    //this->text.setCharacterSize(12);
    this->thubnail.setSize(sf::Vector2f(16,16));
    this->thubnail.setFillColor(this->color);
}

void Player::draw(sf::RenderWindow& window){
    if(this->health > 0){
        window.draw(this->rectangle);
        for(auto & i: this->shoots)
            i.draw(window);
    }
}

bool Player::set_position(BasePlayer& bs){
    //std::cout << "Updating: " << bs.name << "\n";
    this->x =bs.x; this->y = bs.y;
    this->health = bs.health;
    this->kills = bs.kills;
    this->deaths = bs.deaths;
    return true;
}

bool Player::move(int x, int y,Sector& sector){
    if(!sector.check(this->x+x-1,this->y+y-1)){
        this->x += x; this->y += y;
        return true; }
    return false;
}

void Player::control(sf::Time& dt,Sector& sector){
    if (this->timer < 0){
        bool timer{false};
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
            if (this->move(-1,0,sector)) timer = true;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
            if (this->move(1,0,sector)) timer = true;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
            if (this->move(0,-1,sector)) timer = true;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
            if (this->move(0,1,sector)) timer = true;
        }
        std::string space{" "};
         this->move_send = "move "+std::to_string(this->id)+space+std::to_string(this->x)+space+std::to_string(this->y)+space;
        if (timer) this->timer = this->timer_base;
    }
}

void Player::update(sf::Time& dt,Sector& sector){
    int tile_height{32};
    int tile_width{32};
    this->act_y = this->act_y - ((this->act_y - this->y*tile_height)*this->speed*dt.asSeconds());
    this->act_x = this->act_x - ((this->act_x - this->x*tile_width)*this->speed*dt.asSeconds());
    this->rectangle.setPosition(this->act_x,this->act_y);
    for(std::vector<Shoot>::iterator it=this->shoots.begin();
                                                                        it !=this->shoots.end();){
            if(it->update(dt,sector)) it = this->shoots.erase(it);
            else ++it;
    }

    if (this->timer >= 0 ){
        this->timer -= dt.asSeconds();
    }
}

void Player::set_score(BasePlayer & bp){
    this->kills = bp.kills;
    this->deaths = bp.deaths;
}

bool Player::shoot(std::string dir,int x,int y){
    if (this->shoots.size() < this->bullets) {
        if(x == 0 and y == 0) this->shoots.push_back(Shoot(dir,this->x,this->y,this->color));
        else this->shoots.push_back(Shoot(dir,x,y,this->color));
        std::string space{" "};
        this->shoot_send = "shoot "+std::to_string(this->id)+space+std::to_string(this->x)+space+std::to_string(this->y)+space+dir+space;
        return true;
    } return false;
}

bool Player::shoot(BaseShoot& bs){
    if (this->shoots.size() < this->bullets) {
         this->shoots.push_back(Shoot(bs,this->color));
        std::string space{" "};
        return true;
    } return false;
}

std::pair<int,int> Player::check(std::vector<Player>& tp){
    std::pair<int,int> value{this->id,-1};
    for(auto & i : tp){
        for(std::vector<Shoot>::iterator it=this->shoots.begin();
                                                                            it !=this->shoots.end();){
            if(it->x == i.x and it->y == i.y and i.health>0){
                value.second = i.id;
                 it = this->shoots.erase(it);
                return value;
            }
            it++;
        }
    }
    return value;
}

bool Player::gui(unsigned int x, unsigned int y,sf::RenderWindow& window,sf::Text& text){
    if(this->health < 1)
        return false;
    this->bg.setPosition(x,y);
    window.draw(this->bg);

    std::string tmp_string;
    tmp_string += "\t\t " + this->name + "\n";
    tmp_string += "Lives:" + std::to_string(this->health);
    tmp_string += " | Shoots:"+ std::to_string((this->bullets-this->shoots.size()))+"\n";
    tmp_string += "Kills:"+std::to_string(this->kills);
    tmp_string += " | Deaths:"+std::to_string(this->deaths)+"\n";

    text.setString(tmp_string);
    text.setPosition(x+10,y+10);
    this->thubnail.setPosition(x+10,y+10);

    window.draw(text);
    window.draw(this->thubnail);
    return true;
}
