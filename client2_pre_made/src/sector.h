#ifndef SECTOR_H
#define SECTOR_H

#include "main.h"

class Sector{
    public:
        Sector(int, int, unsigned int);
        bool check(int,int);
        void draw(sf::RenderWindow& window);
    private:
        int width;
        int height;
        unsigned int seed;
        sf::Color white{255,255,255};
        sf::Color black{0,0,0};
        sf::RectangleShape rectangle;
        std::vector< std::vector < bool > > tile;
};

#endif
